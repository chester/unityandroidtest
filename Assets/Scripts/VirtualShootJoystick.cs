﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class VirtualShootJoystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    private Image bgImg;
    private Image joystickImg;

    public Vector3 InputDirection { get; set; }

    public Rigidbody palla;

    // Start is called before the first frame update
    void Start()
    {
        bgImg = GetComponent<Image>();
        joystickImg = transform.GetChild(0).GetComponent<Image>();
        InputDirection = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void OnDrag(PointerEventData ped)
    {
        Vector2 pos = Vector2.zero;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(bgImg.rectTransform, ped.position,
            ped.pressEventCamera, out pos))
        {
            pos.x = (pos.x / bgImg.rectTransform.sizeDelta.x);
            pos.y = (pos.y / bgImg.rectTransform.sizeDelta.y);

            float x = (bgImg.rectTransform.pivot.x == 1) ? pos.x * 2 + 1 : pos.x * 2 - 1;
            float y = (bgImg.rectTransform.pivot.y == 1) ? pos.y * 2 + 1 : pos.y * 2 - 1;

            InputDirection = new Vector3(x, 0, y);

            InputDirection = (InputDirection.magnitude > 1) ? InputDirection.normalized : InputDirection;

            joystickImg.rectTransform.anchoredPosition = new Vector3(
                InputDirection.x * (bgImg.rectTransform.sizeDelta.x / 3),
                InputDirection.z * (bgImg.rectTransform.sizeDelta.y / 3));
        }
    }

    public virtual void OnPointerUp(PointerEventData ped)
    {
        Vector3 oldInputDirection = new Vector3(InputDirection.x, InputDirection.y, InputDirection.z);
        InputDirection = Vector3.zero;
        joystickImg.rectTransform.anchoredPosition = Vector3.zero;



        GameObject zandalariTroll = GameObject.FindGameObjectWithTag("Player");


        Rigidbody palla = Instantiate(this.palla);
        palla.tag = "Palla";
        palla.transform.position = new Vector3(zandalariTroll.transform.position.x, zandalariTroll.transform.position.y, (zandalariTroll.transform.position.z + 1.0f));
        palla.transform.rotation = zandalariTroll.transform.rotation;
        palla.gameObject.SetActive(true);

        palla.AddForce(oldInputDirection * 30.0f);

        Destroy(palla.gameObject, 1.5f);
    }

    public virtual void OnPointerDown(PointerEventData ped)
    {
        OnDrag(ped);
    }
}
