﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField]
    private int maxHealth = 100;

    private int currentHealth;

    public event Action<float> OnHealthPctChanged = delegate { };

    private static Animator anim;
    public Rigidbody controller;

    // Start is called before the first frame update
    void Start()
    {
        anim = controller.GetComponent<Animator>();
    }

    void OnEnable()
    {
        currentHealth = maxHealth;
    }

    public int GetCurrentHealth()
    {
        return currentHealth;
    }

    public void ModifyHealth(int amount)
    {
        currentHealth += amount;

        float currentHealthPct = (float) currentHealth / (float) maxHealth;
        OnHealthPctChanged(currentHealthPct);

        if( currentHealthPct <= 0 )
            anim.SetBool("IsDead", true);
    }

    // Update is called once per frame
    void Update()
    {
        if( Input.GetKey(KeyCode.Space))
            ModifyHealth(-1);
    }
}
