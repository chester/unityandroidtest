﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Motor : MonoBehaviour
{
    private static Animator anim;
    public float moveSpeed = 10.0f;

    public float drag = 0.5f;
    public float terminalRotationSpeed = 25.0f;
    public VirtualJoystick moveJoystick;

    private Rigidbody controller;
    private Transform camTransform;


    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<Rigidbody>();
        controller.maxAngularVelocity = terminalRotationSpeed;
        controller.drag = drag;

        camTransform = Camera.main.transform;

        anim = controller.GetComponent<Animator>();

        anim.updateMode = AnimatorUpdateMode.UnscaledTime;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 dir = Vector3.zero;

        dir.x = Input.GetAxis("Horizontal");
        dir.z = Input.GetAxis("Vertical");

        if ( dir.magnitude > 1 )
            dir.Normalize();

        if (moveJoystick.InputDirection != Vector3.zero)
        {
            dir = moveJoystick.InputDirection;
        }

        if( moveJoystick.InputDirection != Vector3.zero 
            || Input.GetKey( KeyCode.W )
            || Input.GetKey( KeyCode.A )
            || Input.GetKey( KeyCode.S )
            || Input.GetKey( KeyCode.D )
            )
            anim.SetBool("IsRunning", true);
        else
            anim.SetBool("IsRunning", false);

        //rotate our direction vector with camera
        Vector3 rotatedDir = camTransform.TransformDirection(dir);
        rotatedDir = new Vector3(rotatedDir.x, 0, rotatedDir.z);
        rotatedDir = rotatedDir.normalized * dir.magnitude;

        controller.freezeRotation = true;
        controller.AddForce(rotatedDir * moveSpeed);
    }
}
